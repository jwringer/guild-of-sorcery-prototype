﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public int Mana = 100;
    public float speed = 1;
    public GameObject firebolt;
    public Camera newCamera;
    public GameObject source;
    public float recharge = 0.1f;
    private float rechargetime = 0.0f;
    public bool canJump = true;
    public float jumpTime = 0.0f;
    private Rigidbody2D rb;
    

    // Use this for initialization
    void Start () {

        rb = GetComponent<Rigidbody2D>();
    

    }
	
	// Update is called once per frame
	void Update () {
        
        

        rechargetime += Time.deltaTime;

        if (rechargetime >= recharge)
        {
            if (Mana < 100)
            {
                rechargetime = 0.0f;
                manaScript.manaValue += 1;
                Mana += 1;
            }
        }

        if (jumpTime < .75)
        {
            canJump = false;
            jumpTime += Time.deltaTime;
        }

        else
        {
            canJump = true;
        }



        if (Input.GetKey(KeyCode.D)){
            
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        }

        if (Input.GetKey(KeyCode.A))
        {
            
            transform.Translate(Vector3.left * Time.deltaTime * speed);
            
            
        }

        if (Input.GetKey(KeyCode.W) && canJump)

        {
            rb.AddForce(new Vector3(0, 5, 0), ForceMode2D.Impulse);
            //transform.Translate(Vector3.up * Time.deltaTime * 15);
            jumpTime = 0;
        }

        if (Input.GetKey(KeyCode.S))
        {
            this.rb.transform.rotation = Quaternion.identity;
        }


        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePoint = newCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector3 difference = mousePoint - transform.position;
            difference.Normalize();
            float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            Quaternion newRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, rotZ));
            transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, Time.deltaTime * 1);

            GameObject FireboltHandler;
            FireboltHandler = Instantiate(firebolt, source.transform.position, newRotation) as GameObject;
            //placeholder
            Mana -= 5;
            manaScript.manaValue -= 5;
            Destroy(FireboltHandler, 0.3f);
            
        }
    }
}
