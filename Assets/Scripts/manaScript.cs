﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class manaScript : MonoBehaviour {
    public static int manaValue = 100;
    Text mana;
    
	// Use this for initialization
	void Start () {
        mana = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

        mana.text = "Mana: " + manaValue;
            
	}
}
